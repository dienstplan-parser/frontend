set -e
if [ -z "$1" ]
then
  echo "Please provide tag to build docker image for!"
  exit 1
fi

set_color (){
 tput setaf 2
 tput bold
}

reset_color (){
  tput sgr0
}

echo_colored (){
  set_color
  echo "$1"
  reset_color
}

echo_colored "Checking out tag"
git checkout tags/"$1"
export VITE_APP_VERSION=$1

echo_colored "Running vite build"
npm run build

echo_colored "Building docker image"
docker build -t dienstplan-parser-frontend:"$1" .