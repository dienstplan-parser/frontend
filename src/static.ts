export const TLS_ENABLED: boolean = import.meta.env.VITE_TLS_ENABLED === "true";
export const API_HOST: string = (TLS_ENABLED ? "https://" : "http://") + import.meta.env.VITE_API_HOST;
export const WS_HOST: string = (TLS_ENABLED ? "wss://" : "ws://") + import.meta.env.VITE_API_HOST;
export const MAX_FILE_SIZE_BYTES: number = parseInt(import.meta.env.VITE_MAX_FILE_SIZE_BYTES);

export const PAGE_NAME = "Dienstplan2Calendar";
export const HOST = "dienstplan.preisler.lol";
export const REPO = "https://gitlab.com/dienstplan-parser/";