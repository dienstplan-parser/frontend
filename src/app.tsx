import {Link, Route, useLocation, useResolvedPath, useRoutes} from "solid-app-router";
import {routes} from "./routes";
import {createMemo, createSignal, onMount, Show} from "solid-js";
import {API_HOST, HOST, PAGE_NAME, REPO} from "./static";
import ApiHealth from "./components/ApiHealth";
import LabelChip, {LabelChipDiv} from "./components/LabelChip";
import ArrowRightIcon from "./components/icon/ArrowRightIcon";


const REFRESH_DELAY = 1000 * 60;

export default () => {
    const Route = useRoutes(routes);
    const [apiHealth, setApiHealth] = createSignal(true);

    onMount(async () => {
        const checkState = async () => {
            try {
                const response = await fetch(API_HOST + "/health", {method: "HEAD"});
                setApiHealth(response.status === 200);
            } catch (e) {
                setApiHealth(false);
            }
        };

        await checkState();

        setTimeout(async () => {
            await checkState();
        }, REFRESH_DELAY);
    });

    return (
        <div class="flex flex-col min-h-screen bg-white">
            <nav class="navbar text-neutral-content">
                <div class="flex flex-none items-center px-2 mx-2 space-x-1">
                    <Link href="/">
                        <span class="no-underline text-lg text-black font-bold">{PAGE_NAME}</span>
                    </Link>
                    <LabelChip text={import.meta.env.VITE_APP_VERSION} noBottomPadding={true}/>
                </div>
            </nav>

            <main class="flex-grow">
                <div class="md:container md:mx-auto mt-10 p-3">
                    <Show when={apiHealth()} fallback={<ApiHealth/>}>
                        <Route/>
                    </Show>
                </div>
            </main>

            <footer class="pb-5 footer footer-center">
                <div>
                    <div class="grid grid-flow-col gap-2">
                        <FooterChip href="/" text="Home"/>
                        <FooterChip href="/privacy" text="Privacy"/>
                        <a href={REPO} target="_blank">
                            <LabelChipDiv
                                className="flex items-center border-2 border-transparent">
                                Demo & Source
                                <ArrowRightIcon width="w-4" height="h-4"/>
                            </LabelChipDiv>
                        </a>
                    </div>
                    <span class="font-medium">© 2022 - {HOST}</span>
                </div>
            </footer>
        </div>
    );
};

function FooterChip(props) {
    //taken from https://github.com/solidjs/solid-app-router/blob/4fe17f484cd74191d95d1f4c278928afb9f73267/src/components.tsx#L209
    const location = useLocation();
    const to = useResolvedPath(() => props.href);
    const isActive = createMemo(() => {
        const to_ = to();
        if (to_ === undefined) {
            return false;
        }
        const path = to_.split(/[?#]/, 1)[0].toLowerCase();
        const loc = location.pathname.toLowerCase();
        return path === loc;
    });

    return (
        <Link href={props.href}>
            <div className="flex mb-2 items-center justify-between">
                <LabelChipDiv classList={{"bg-pink-200": isActive()}} className={"border-2 border-pink-200 hover:bg-pink-200"}>
                    {props.text}
                </LabelChipDiv>
            </div>
        </Link>
    );
}
