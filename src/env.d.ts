interface ImportMetaEnv {
    VITE_API_HOST: string;
    VITE_TLS_ENABLED: string;
    VITE_MAX_FILE_SIZE_BYTES: string;
    VITE_APP_VERSION: string;
}