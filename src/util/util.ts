import {getMonthName} from "./date";

export const delay = (ms: number, fn: () => void) => {
    let interval = setInterval(() => {
        fn();
        window.clearInterval(interval);
    }, ms);
};

export const arrayContains = (array: string[], search: string) => {
    for (let item of array) {
        if (item === search) return true;
    }

    return false;
};

export const FILE_NAME_BASE = "dienstplan"

export const getFileName = (month: number, year: number) => `${FILE_NAME_BASE}-${getMonthName(month - 1)}-${year}.ics`;