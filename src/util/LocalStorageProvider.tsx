import {Component, createContext, useContext} from "solid-js";
import {createStore, Part, Store} from "solid-js/store";

export type DataStore = {
    recents: Array<{ key: string, addedAt: number, month: number, year: number, value: string }>,
};

export const EMPTY_DATASTORE = {
    recents: []
} as DataStore

export const RECENT_SCANS_KEY = "recent_scans";

export type LocalStorageStore = [
    DataStore,
    {
        write?: () => void;
        read?: () => DataStore;
        modify?: (key: Part<DataStore>, modifier: (data: any) => any) => void;
    }
];

const LocalStorageContext = createContext<LocalStorageStore>([
    EMPTY_DATASTORE, {}
]);

export const LocalStorageProvider: Component<{ key: string, data: DataStore }> = (props) => {
    const [state, setState] = createStore((JSON.parse(localStorage.getItem(props.key)) as DataStore) || props.data || EMPTY_DATASTORE),
        store: LocalStorageStore = [
            state, {
                write() {
                    localStorage.setItem(props.key, JSON.stringify(state))
                },
                read() {
                    return JSON.parse(localStorage.getItem(props.key)) || null;
                },
                modify(key: Part<DataStore>, modifier: (current: any) => any) {
                    setState(key, (current) => modifier(current));
                }
            }
        ];

    return (
        <LocalStorageContext.Provider value={store}>
            {props.children}
        </LocalStorageContext.Provider>
    );
};

export function useLocalStorageContext() {
    return useContext(LocalStorageContext);
}