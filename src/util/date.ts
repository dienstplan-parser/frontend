const YEAR = new Date().getFullYear();

export function getMonthList(): string[] {
    return [...Array(12).keys()].map(idx => getMonthName(idx));
}

export function getMonthName(idx: number): string {
    return new Date(YEAR, idx).toLocaleString("default", {month: "long"});
}