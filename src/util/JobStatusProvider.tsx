import {ApiJobDataV1} from "../rest/generated/ApiJobDataV1";
import {Component, createContext, useContext} from "solid-js";
import {createStore} from "solid-js/store";
import {ApiCalendarCreatedWrapped} from "../rest/ApiCalendarCreatedWrapped";
import {ApiJobFailureDataV1} from "../rest/generated/ApiJobFailureDataV1";

export type JobStatusStore = [
    { componentState: number, backendState: string, jobData: ApiJobDataV1 | ApiJobFailureDataV1, calendarData: ApiCalendarCreatedWrapped },
    {
        updateComponentState?: (newState: number) => void;
        updateBackendState?: (newState: string) => void;
        updateJobData?: (newJobData: ApiJobDataV1 | ApiJobFailureDataV1) => void;
        updateCalendar?: (calendarData: ApiCalendarCreatedWrapped) => void;
    }
];

const JobStatusContext = createContext<JobStatusStore>([
    {componentState: 0, backendState: "Queued", jobData: null, calendarData: null}, {}
]);

export const JobStatusProvider: Component<{ componentState: 0, backendState: string,
    jobData: ApiJobDataV1 | ApiJobFailureDataV1, calendarData: ApiCalendarCreatedWrapped }> = (props) => {
    const [state, setState] = createStore({
            componentState: props.componentState || 0,
            backendState: props.backendState || null,
            jobData: props.jobData || null,
            calendarData: props.calendarData || null
        }),
        store: JobStatusStore = [
            state, {
                updateComponentState(newState: number){
                    setState("componentState", newState);
                },
                updateBackendState(newState: string) {
                    setState("backendState", (current) => newState);
                },
                updateJobData(newJobData: ApiJobDataV1 | ApiJobFailureDataV1) {
                    setState("jobData", (current) => newJobData);
                },
                updateCalendar(newCalendar: ApiCalendarCreatedWrapped) {
                    setState("calendarData", (current) => newCalendar);
                }
            }
        ];

    return (
        <JobStatusContext.Provider value={store}>
            {props.children}
        </JobStatusContext.Provider>
    );
};

export function useJobStatus() {
    return useContext(JobStatusContext);
}