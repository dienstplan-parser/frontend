import ErrorIcon from "./icon/ErrorIcon";

export default function ApiHealth() {
    return (
        <div class="flex justify-center">
            <div class="flex justify-center items-center rounded-xl bg-red-500 text-white px-3 py-2">
                <ErrorIcon className="mr-3"/>
                <div>
                    <div className="font-bold">An API error has been detected.</div>
                    <div>Please check back later!</div>
                </div>
            </div>
        </div>
    );
};
