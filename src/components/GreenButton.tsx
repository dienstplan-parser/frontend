import {ButtonProps} from "./ButtonProps";
import {Show} from "solid-js";

export default function GreenButton(props: ButtonProps) {
    return (
        <button disabled={props.disabled} type={props.type} onClick={props.onClick}
                class="flex justify-center items-center bg-green-500 disabled:bg-gray-200 text-white disabled:text-gray-500 active:bg-green-600
                 font-bold uppercase text-xs px-4 py-2 rounded-full shadow disabled:shadow-none hover:shadow-md hover:bg-green-400 outline-none
                 focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:cursor-default">
            <Show when={props.iconRight} fallback={props.icon}>
                {props.text}
            </Show>

            <Show when={props.iconRight} fallback={<span>{props.text}</span>}>
                {props.icon}
            </Show>
        </button>
    );
};