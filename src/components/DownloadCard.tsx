import DownloadIcon from "./icon/DownloadIcon";
import {Link} from "solid-app-router";
import {JSX} from "solid-js";


export type DownloadCardProps = {
    headingText: string,
    bodyText: string,
    calendarData: string
    fileName: string,
    children?: JSX.Element
}

export default (props: DownloadCardProps) => {
    return (
        <div
            class="shadow-xl text-white px-6 pt-5 border-0 rounded-2xl relative mb-4 bg-gradient-to-r from-purple-400 via-pink-600 to-red-400">
                <span class="mr-8">
                    <h1 class="text-lg font-semibold">{props.headingText}</h1>
                    <p class="mb-2">{props.bodyText}</p>

                    {props.children}

                    <a href={"data:text/calendar;charset=utf-8," + encodeURIComponent(props.calendarData)}
                       download={props.fileName}
                       class="flex justify-center items-center text-white bg-transparent border border-solid border-white hover:bg-purple-400
                             font-bold uppercase text-xs px-4 py-2 rounded-full outline-none focus:outline-none ease-linear transition-all duration-150 hover:cursor-pointer">
                        <DownloadIcon className="mr-1"/>
                        Download
                    </a>
                </span>
            <Link href="/" replace={true}>
                <button
                    class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
                    <span>×</span>
                </button>
            </Link>
        </div>
    );
}