import {Link} from "solid-app-router";

export default function RedErrorCard(props) {
    return (
        <div class="shadow-xl text-white px-6 pt-5 border-0 rounded-2xl relative mb-4 bg-red-500">
            <span class="mr-8">{props.children}</span>

            <Link href="/" replace={true}>
                <button
                    class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
                    <span>×</span>
                </button>
            </Link>
        </div>
    );
};