import {JSX, Show} from "solid-js";
import {ButtonProps} from "./ButtonProps";

export default function TinyRoundButton(props: ButtonProps) {
    return (
        <button class="flex justify-center items-center text-pink-500 bg-transparent border border-solid border-pink-500 hover:bg-pink-500 hover:text-white
            active:bg-pink-600 font-bold uppercase text-xs px-4 py-2 rounded-full outline-none focus:outline-none mr-1 mb-1
            ease-linear transition-all duration-150" type="button" onClick={props.onClick}>

            <Show when={props.iconRight} fallback={props.icon}>
                {props.text}
            </Show>

            <Show when={props.iconRight} fallback={<span>{props.text}</span>}>
                {props.icon}
            </Show>
        </button>
    )
}