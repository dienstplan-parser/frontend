import {JSX} from "solid-js";

interface MaxWidthHelperProps {
    children: JSX.Element
    size?: "md" | "xs"
}

const __force_compile = "max-w-md";
const __force_compile_2 = "max-w-xs";

export default function MaxWidthHelper(props: MaxWidthHelperProps) {
    return (<div class={`max-w-${props.size ?? "md"} mx-auto`}>{props.children}</div>);
};