import {Show} from "solid-js";
import {FormControl, withControl} from "rx-controls-solid";
import {toSignal} from "../utils";

export const TextField = withControl<{
    control: FormControl<string | null>,
    ref?: HTMLInputElement | undefined,
    controlName: string,
    type: "text" | "password" | "number",
    name: string,
    placeholder?: string,
    required: boolean,
}>((props) => {
    const control = props.control;
    const error = toSignal(control.observe("errors"));

    return (
        <>
            <div className="mb-3">
                <input onInput={(e) => {
                    control.markDirty(true);
                    control.setValue(e.currentTarget.value);
                }}
                       type={props.type} name={props.name}
                       placeholder={props.placeholder}
                       value={control.value} required={props.required}
                       classList={{
                           "border-red-500": error() === undefined,
                           "focus:border-pink-500": !error(),
                       }}
                       onBlur={() => control.markTouched(true)}
                       ref={props.ref}
                       className="px-3 py-3 mb placeholder-blueGray-300 text-black relative bg-white rounded-lg text-sm focus:outline-none w-full border-2"/>
                <Show when={error()}>
                        <span
                            className="text-xs text-red-500">{error().message || "Line does not match any format!"}</span>
                </Show>
            </div>
        </>
    );
});