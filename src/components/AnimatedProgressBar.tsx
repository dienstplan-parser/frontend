import {keyframes, styled} from "solid-styled-components";

export const AnimatedProgressBar = styled("div")`
        & {
            position: relative;
            overflow: hidden;
        }
        
        &:after {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            transform: translateX(-100%);
            background-image: linear-gradient(
                90deg,
                rgba(255, 255, 255, 0.4) 0,
                rgba(255, 255, 255, 0.3) 50%,
                rgba(255, 255, 255, 0.2) 100%
              );
            animation: ${keyframes`
              100% {
                transform: translateX(0%);
                opacity: 0;
              }
            `} 2s ease-out infinite;
            content: "";
        }   
    `;