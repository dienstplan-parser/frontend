import {JSX, Show} from 'solid-js';

interface LabelChipProps {
    text: string,
    textRight?: JSX.Element
    noBottomPadding?: boolean
}

export default function LabelChip(props: LabelChipProps) {
    return (
        <div class="flex items-center justify-between" classList={{
            "mb-2": !props.noBottomPadding
        }}>
            <LabelChipDiv
                className="bg-pink-200">
                {props.text}
            </LabelChipDiv>

            <Show when={props.textRight}>
                <div class="text-right">
                    <span class="text-xs font-semibold inline-block text-pink-600">{props.textRight}</span>
                </div>
            </Show>
        </div>
    )
}

export type LabelChipDivProps = {
    className?: string,
    classList?: { [k: string]: boolean; },
    children?: JSX.Element
}

export function LabelChipDiv(props: LabelChipDivProps) {
    return (
        <div
            class="text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-pink-600 "
            className={props.className}
            classList={props.classList || {}}>
            {props.children}
        </div>
    );
}
