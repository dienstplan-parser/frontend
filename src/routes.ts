import {lazy} from "solid-js";
import {RouteDefinition} from "solid-app-router";

import Home from "./pages/home/home";
import DownloadData from "./pages/download/[id].data";

export const routes: RouteDefinition[] = [
    {
        path: "/",
        component: Home,
    },
    {
        path: "/job/:id",
        component: lazy(() => import("./pages/job/[id]")),
        // data: JobData
    },
    {
        path: "/d/:id",
        component: lazy(() => import("./pages/download/[id]")),
        data: DownloadData,
    },
    {
        path: "/privacy",
        component: lazy(() => import("./pages/privacy/privacy")),
    },
    {
        path: "**",
        component: lazy(() => import("./errors/404")),
    }

    // {
    //   path: "/about",
    //   component: lazy(() => import("./pages/about")),
    //   data: AboutData,
    // },
    // {
    //   path: "**",
    //   component: lazy(() => import("./errors/404")),
    // },
];
