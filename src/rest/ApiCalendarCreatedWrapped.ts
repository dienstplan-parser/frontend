import {ApiCalendarCreatedResponseV1} from "./generated/ApiCalendarCreatedResponseV1";
import {ApiCalendarCreatedResponseV2} from "./generated/ApiCalendarCreatedResponseV2";

export class ApiCalendarCreatedWrapped {
    calendar: ApiCalendarCreatedResponseV2;
    month: number;
    year: number;

    constructor(calendar: ApiCalendarCreatedResponseV2, month: number, year: number) {
        this.calendar = calendar;
        this.month = month;
        this.year = year;
    }

}