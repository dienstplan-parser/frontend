import {createSignal, onCleanup} from "solid-js";

import {Observable} from "rxjs";
import * as Util from "util";

export function toSignal<T>(input: Observable<T>, defaultValue?: T) {
    const [value, setValue] = createSignal(defaultValue as T);

    const sub = input.subscribe(
        // @ts-ignore
        setValue
    );

    // const sub = input.subscribe((v) => setValue(v as T));
    onCleanup(() => sub.unsubscribe());

    return value;
}

const KILO_BYTES: number = 1024;
const MEGA_BYTES: number = KILO_BYTES * KILO_BYTES;
const GIGA_BYTES: number = MEGA_BYTES * KILO_BYTES;
const UNITS = new Map<string, number>([["kb", KILO_BYTES], ["mb", MEGA_BYTES], ["gb", GIGA_BYTES]])

export function bytesToUnit(bytes: number): string {
    let lastKey = null;
    for (let [key, value] of UNITS) {
        if (bytes < value) {
            if (lastKey === null) {
                return `${bytes}${key}`;
            }

            return `${(bytes / UNITS.get(lastKey)).toFixed(2)}${lastKey}`;
        }

        lastKey = key;
    }
}