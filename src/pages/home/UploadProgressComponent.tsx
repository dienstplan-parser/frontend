import LabelChip from "../../components/LabelChip";
import TinyRoundButton from "../../components/TinyRoundButton";
import {Accessor} from "solid-js";

export default function UploadProgressComponent(uploadProgress: Accessor<string>, cancelUpload: () => void) {
    return (
        <div class="relative pt-1">
            <LabelChip text="Upload in progress" textRight={uploadProgress()}/>

            <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-pink-200">
                <div style={{width: uploadProgress()}}
                     class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-pink-500"/>
            </div>

            <TinyRoundButton text="Cancel" onClick={cancelUpload}/>
        </div>
    );
}