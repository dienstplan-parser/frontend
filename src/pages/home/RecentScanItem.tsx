import DownloadIcon from "../../components/icon/DownloadIcon";
import CloseIcon from "../../components/icon/CloseIcon";
import {useLocalStorageContext} from "../../util/LocalStorageProvider";

function RoundedButton(props) {
    return (
        <a href={props.href} onclick={props.onclick} download={props.download}
           class="flex justify-center items-center rounded-full bg-gray-200 text-black hover:cursor-pointer"
           style="width: 1.4rem; height:1.4rem"
        >{props.children}</a>
    );
}

export default function RecentScanItem({recent}: {
    recent: { key: string, addedAt: number, month: number, year: number, value: string }
}) {
    const [_, {write, modify}] = useLocalStorageContext();

    return (
        <div class="flex justify-between items-center mb-1 border-2 rounded-lg px-2 py-2">
            <div class="w-9/12">
                <p class="">{recent.key}</p>
                <p class="text-sm text-gray-500">{new Date(recent.addedAt).toLocaleString()}</p>
            </div>
            <div class="flex space-x-1">
                <RoundedButton href={"data:text/calendar;charset=utf-8," + encodeURIComponent(recent.value)}
                               download={recent.key}>
                    <DownloadIcon/>
                </RoundedButton>

                <RoundedButton href="#" onclick={() => {
                    modify("recents", (current) => current.filter(obj => obj.key !== recent.key));
                    write();
                }}>
                    <CloseIcon/>
                </RoundedButton>
            </div>
        </div>
    );
};