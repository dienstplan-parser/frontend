import {createSignal} from "solid-js";
import {Dynamic} from "solid-js/web";
import {useNavigate} from "solid-app-router";
import {ApiJobCreatedResponseV1} from "../../rest/generated/ApiJobCreatedResponseV1";
import UploadProgressComponent from "./UploadProgressComponent";
import UploadButtonComponent from "./UploadButtonComponent";
import {API_HOST} from "../../static";
import MaxWidthHelper from "../../components/MaxWidthHelper";

export default function Home() {
    const navigate = useNavigate();

    const [childComponent, setChildComponent] = createSignal(null);
    const [uploadProgress, setUploadProgress] = createSignal("0%");

    let xhr = null;

    const upload = (formData: FormData) => {
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = (evt: Event) => {};

        xhr.upload.addEventListener("progress", (evt: ProgressEvent<XMLHttpRequestEventTarget>) => {
            if (evt.lengthComputable) {
                setUploadProgress(Math.trunc(evt.loaded / evt.total * 100) + "%");
            }
        }, false);

        xhr.onloadend = (evt: ProgressEvent) => {
            if (xhr.status === 200) {
                let uploadResponseJson = JSON.parse(xhr.response);
                let apiJobCreated = uploadResponseJson as ApiJobCreatedResponseV1;

                navigate("/job/" + apiJobCreated.metadata.id, {
                    replace: true
                })
            }
        };

        xhr.open("POST", API_HOST + "/job");
        xhr.send(formData);

        setChildComponent(UploadProgressComponent(uploadProgress, cancelUpload));
    };

    const cancelUpload = () => {
        if (xhr !== null) {
            xhr.abort();
            reset();
        }
    };

    const reset = () => {
        resetComponent();
        setUploadProgress("0%");
    };

    const resetComponent = () => setChildComponent(UploadButtonComponent({requestUploadStart: upload}));

    resetComponent();

    return (
        <MaxWidthHelper size="xs">
            <Dynamic component={() => childComponent}/>
        </MaxWidthHelper>
    );
}
