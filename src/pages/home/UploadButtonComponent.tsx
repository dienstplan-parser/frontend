import {arrayContains, delay} from "../../util/util";
import {createSignal, Index, Show} from "solid-js";
import UploadIcon from "../../components/icon/UploadIcon";
import TinyRoundButton from "../../components/TinyRoundButton";
import GreenButton from "../../components/GreenButton";
import ArrowRightIcon from "../../components/icon/ArrowRightIcon";
import {css, keyframes} from "solid-styled-components";
import RecentScanItem from "./RecentScanItem";
import RecentScanCard from "./RecentScanCard";
import {LocalStorageProvider} from "../../util/LocalStorageProvider";
import {Link} from "solid-app-router";
import {bytesToUnit} from "../../utils";
import {MAX_FILE_SIZE_BYTES} from "../../static";

const shakeAnimation = keyframes`
    0% { transform: translate(30px); }
    20% { transform: translate(-30px); }
    40% { transform: translate(15px); }
    60% { transform: translate(-15px); }
    80% { transform: translate(8px); }
    100% { transform: translate(0px); }
`

const shakeErrorClass = css`
    animation: ${shakeAnimation} 0.4s 1 linear;
`

const PRIVACY_STATE_AWAITING_FILE_SELECTION: number = 1;
const PRIVACY_STATE_REQUEST_MODAL_OPEN: number = 2;
const PRIVACY_STATE_CONFIRMED: number = 3;
const PRIVACY_STATE_DENIED: number = 4;

const SUPPORTED_FORMATS = ["image/png", "image/jpg", "image/jpeg", "application/pdf"];

export default function UploadButtonComponent({requestUploadStart}: {
    requestUploadStart: (formData: FormData) => void
}) {
    const [fileError, setFileError] = createSignal(null);
    const [privacyState, setPrivacyState] = createSignal(PRIVACY_STATE_AWAITING_FILE_SELECTION);

    let fileAwaitingUpload = null;

    const startUpload = (file: File) => {
        if (file.size > MAX_FILE_SIZE_BYTES) {
            setFileError(`file too big, max allowed size is ${maxFileSize}`);
            return;
        }

        const fd = new FormData();
        fd.append("file", file);
        fd.append("privacy_consent", "true");

        requestUploadStart(fd);
    };

    function checkFormat(files: FileList | null) {
        if (files.length >= 1 && arrayContains(SUPPORTED_FORMATS, files[0].type)) {
            requestPrivacyPermission(files[0]);
        } else {
            setFileError("invalid file format");
        }
    }

    const requestPrivacyPermission = (file) => {
        fileAwaitingUpload = file;
        setPrivacyState(PRIVACY_STATE_REQUEST_MODAL_OPEN);
    };

    const handleDragOver = (event) => {
        event.preventDefault();
        divUploadButton.classList.add("bg-pink-300");
    };

    const handleDragExit = () => {
        divUploadButton.classList.remove("bg-pink-300");
    };

    let divUploadButton;
    let inputFileUpload;
    let modalPrivacy;

    const maxFileSize = bytesToUnit(MAX_FILE_SIZE_BYTES);

    return (<>
        <div ref={divUploadButton} onClick={() => inputFileUpload.click()}
             class="group border-2 text-pink-500 py-10 rounded-lg hover:bg-pink-300 cursor-pointer"
             classList={{
                 [shakeErrorClass]: fileError(),
                 "border-red-500": fileError(),
                 "border-pink-500": !fileError()
             }}
             ondrop={(event) => {
                 event.preventDefault();
                 checkFormat(event.dataTransfer.files);
             }}
             ondragover={handleDragOver}
             ondragexit={handleDragExit}
             onanimationend={(e) => {
                 if (e.animationName === shakeAnimation) {
                     delay(1000, () => setFileError(null));
                 }
             }}>
            <div class="flex justify-center items-center">
                <Show when={!fileError()}
                      fallback={<p class="flex justify-center text-red-500">{fileError()}</p>}>
                    <UploadIcon className="group-hover:text-white"/>
                    <p class="group-hover:text-white cursor-pointer">drop or click to upload</p>
                </Show>
            </div>
        </div>

        <input type="file" id="file" accept="image/*|application/pdf" ref={inputFileUpload}
               onChange={(event) => checkFormat(inputFileUpload.files)}
               capture="environment" class="hidden"/>

        <p class="mt-1 italic text-xs text-pink-500">Supported formats: png, jpg, jpeg, pdf</p>
        <p class="italic text-xs text-pink-500">{`Max filesize: ${maxFileSize}`} </p>

        <div class="modal" ref={modalPrivacy} classList={
            {"modal-open": privacyState() == PRIVACY_STATE_REQUEST_MODAL_OPEN}
        }>
            <div class="modal-box">
                <h2 class="card-title">Privacy notice</h2>
                <p class="font-medium">By clicking "I agree", you agree to our <a href="/privacy" target="_blank"><span
                    class="underline">privacy policy</span></a>.
                </p>

                <details className="mt-1">
                    <summary><span className="font-medium cursor-pointer">Summary</span></summary>
                    <p className="mt-1">You agree to the selected
                        image/file being uploaded
                        and processed on our
                        server,
                        where it is
                        temporarily stored so we can scan its contents using text-recognition software; It is
                        subsequently
                        deleted. While this process
                        usually
                        takes less then 30 seconds, it might be possible that your image and/or its contents are stored
                        for
                        up to 7 days (or parts of its contents might appear in log-files). Additionally, the generated calendar
                        file is stored for <span class="font-semibold">1 hour</span> so it can be easily accessed from a generated quicklink.</p>
                </details>

                <div class="modal-action">
                    <TinyRoundButton text="Cancel" onClick={() => setPrivacyState(PRIVACY_STATE_DENIED)}/>
                    <GreenButton text="I agree" onClick={() => {
                        setPrivacyState(PRIVACY_STATE_CONFIRMED);
                        startUpload(fileAwaitingUpload);
                    }} icon={ArrowRightIcon({width: "w-4", height: "w-4"})} iconRight={true}/>
                </div>
            </div>
        </div>

        <RecentScanCard className="mt-4"/>
    </>)
}