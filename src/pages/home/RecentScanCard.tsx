import LabelChip from "../../components/LabelChip";
import {
    EMPTY_DATASTORE,
    LocalStorageProvider,
    RECENT_SCANS_KEY,
    useLocalStorageContext
} from "../../util/LocalStorageProvider";
import RecentScanItem from "./RecentScanItem";
import {For, Show} from "solid-js";


function RecentScanCardInternal(props) {
    const [state, {write, modify}] = useLocalStorageContext();

    return (
        <Show when={state.recents.length > 0}>
            <div className={props.className}>
                <LabelChip text="Recent scans" textRight={
                    <button class="font-semibold" onclick={() => {
                        modify("recents", () => []);
                        write();
                    }}>Clear all</button>
                }/>
                <ul>
                    <li>
                        <For each={[...state.recents].sort((a, b) => b.addedAt - a.addedAt)}>{(item, i) =>
                            <RecentScanItem recent={item}/>
                        }
                        </For>
                    </li>
                </ul>
            </div>
        </Show>
    );
}


export default function RecentScanCard(props) {
    return (
        <LocalStorageProvider key={RECENT_SCANS_KEY} data={EMPTY_DATASTORE}>
            <RecentScanCardInternal {...props}/>
        </LocalStorageProvider>
    );
};