import {Link} from "solid-app-router";
import {HOST, PAGE_NAME} from "../../static";

interface ParagraphProps {
    id: string,
    text: string
}

function ParagraphHeading(props: ParagraphProps) {
    return (
        <h2 class="text-xl mt-4" id={props.id}><Anchor id={props.id} text={props.text}/></h2>
    );
}

function Anchor(props: ParagraphProps) {
    return (<a class="link link-hover" href={props.id}>{props.text}</a>);
}

export default () => {
    return (
        <div>
            <h1 class="text-2xl">Privacy Policy for {PAGE_NAME}</h1>

            <p>At {PAGE_NAME}, accessible from {HOST}, one of our main priorities is the privacy of our visitors. This
                Privacy Policy document contains types of information that is collected and recorded by {PAGE_NAME} and
                how we
                use it.</p>

            <p>If you have additional questions or require more information about our Privacy Policy, do not hesitate to
                contact us.</p>

            <p>This Privacy Policy applies to our online activities and is valid for visitors to our website with
                regards to the information that they shared and/or collect in {PAGE_NAME}.</p>

            <ParagraphHeading id={"consent"} text={"Consent"}/>

            <p>By using our website, you hereby consent to our Privacy Policy and agree to its terms.</p>

            <ParagraphHeading id={"log-files"} text={"Log files"}/>

            <p>{PAGE_NAME} follows a standard procedure of using log files. These files log visitors when they visit
                websites.
                All hosting companies do this and a part of hosting services' analytics. The information collected by
                log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date
                and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any
                information that is personally identifiable. The purpose of the information is for analyzing trends,
                administering the site, tracking users' movement on the website, and gathering demographic
                information.</p>

            <ParagraphHeading id={"data-collected"} text={"Data collected"}/>

            <p>When using the "Dienstplan scanning" function of our webpage, the image you select is uploaded to our
                server, where it is <span class="font-semibold">temporarily</span> stored, so its contents can be
                analyzed using text-recognition software. This process usually takes less than 30 seconds, after which
                the uploaded image is automatically deleted again. However, it might be possible for the uploaded image
                to be stored up to 7 days (in case of some kind server-side error). Additionally, the generated calendar
                file is stored for <span class="font-semibold">1 hour</span> so it can be easily accessed from a generated quicklink.</p>

            <ParagraphHeading id={"use-of-data"} text={"Data usage"}/>

            <p>As already explained in "<Anchor id={"data-collected"} text={"Data collected"}/>" all data provided by
                the websites users is only used for its intended purpose, and is subsequently deleted.</p>
        </div>
    );
};
