import {Link, useData} from "solid-app-router";
import MaxWidthHelper from "../../components/MaxWidthHelper";
import DownloadIcon from "../../components/icon/DownloadIcon";
import {Switch, Match, Show, Resource, createEffect} from "solid-js";
import {ApiQuickLinkResponseV2} from "../../rest/generated/ApiQuickLinkResponseV2";
import {getFileName} from "../../util/util";
import DownloadCard from "../../components/DownloadCard";
import RedErrorCard from "../../components/RedErrorCard";


export default function Download() {
    const calendar = useData<Resource<ApiQuickLinkResponseV2 | null>>();
    const fileName = () => getFileName(calendar().month, calendar().year);

    return (
        <MaxWidthHelper size="md">
            <Switch>
                <Match when={calendar() === null}>
                    <RedErrorCard>
                        <h1 class="text-lg font-semibold">No such download link found :/</h1>
                        <p>This link either never existed, has already expired, or you just mistyped.</p>
                    </RedErrorCard>
                </Match>
                <Match when={!calendar.loading}>
                    <DownloadCard headingText="Download calendar events"
                                  bodyText="Calendar events have been generated and can be downloaded using the button below. The file can be imported by most (if not all) calendar applications."
                                  calendarData={calendar().text} fileName={fileName()}/>
                </Match>
            </Switch>
        </MaxWidthHelper>
    );
}