import {RouteDataFuncArgs} from "solid-app-router";
import {create} from "domain";
import {createResource, Resource} from "solid-js";
import {API_HOST} from "../../static";
import {ApiQuickLinkResponseV2} from "../../rest/generated/ApiQuickLinkResponseV2";

export default function DownloadData(args: RouteDataFuncArgs): Resource<ApiQuickLinkResponseV2 | null> {
    const [calendar] = createResource<ApiQuickLinkResponseV2, string>(() => args.params.id, async (id) => {
        const resp = await fetch(`${API_HOST}/calendar/${id}`);
        if (resp.status === 200) {
            return (await resp.json()) as ApiQuickLinkResponseV2;
        }

        return null;
    });

    return calendar;
}