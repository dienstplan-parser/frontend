import LabelChip from "../../components/LabelChip";
import {AnimatedProgressBar} from "../../components/AnimatedProgressBar";
import {useJobStatus} from "../../util/JobStatusProvider";

export default function JobBackendComponent() {
    const [state, {}] = useJobStatus();

    return (
        <div class="max-w-xs mx-auto">
            <div class="relative pt-1">
                <LabelChip text={state.backendState} textRight="This might take a few seconds"/>

                <AnimatedProgressBar
                    className="bg-pink-200 top-0 h-2 rounded bg-gradient-to-r from-blue-100 to-orange-400 via-purple-400"/>
            </div>
        </div>
    );
};