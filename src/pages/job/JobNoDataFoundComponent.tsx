import MaxWidthHelper from "../../components/MaxWidthHelper";
import RedErrorCard from "../../components/RedErrorCard";

export default function JobNoDataFoundComponent() {
    return (
        <MaxWidthHelper>
            <RedErrorCard>
                <h1 class="text-lg font-semibold">No data found :(</h1>
                <p>This does image does not seem to be a Dienstplan since no data could be found. Try again?</p>
            </RedErrorCard>
        </MaxWidthHelper>
    );
};