import {getMonthName} from "../../util/date";
import DownloadIcon from "../../components/icon/DownloadIcon";
import {useJobStatus} from "../../util/JobStatusProvider";
import {Link} from "solid-app-router";
import {
    EMPTY_DATASTORE,
    LocalStorageProvider,
    RECENT_SCANS_KEY,
    useLocalStorageContext
} from "../../util/LocalStorageProvider";
import MaxWidthHelper from "../../components/MaxWidthHelper";
import {API_HOST, HOST} from "../../static";
import {ApiCalendarCreatedResponseV1} from "../../rest/generated/ApiCalendarCreatedResponseV1";
import {getFileName} from "../../util/util";
import DownloadCard from "../../components/DownloadCard";

function GenerateComponentInternal(props) {
    const [state, {}] = useJobStatus();
    const [_, {write, modify}] = useLocalStorageContext();

    const calendarData = state.calendarData;
    const fileName = getFileName(calendarData.month, calendarData.year);
    const quickUrl = `https://${HOST}/d/${calendarData.calendar.quicklinkToken}`;

    modify("recents", (data) => {
        const obj = data.find(obj => obj.key === fileName);
        if (obj === undefined) {
            data.push({
                key: fileName,
                addedAt: Date.now(),
                month: calendarData.month,
                year: calendarData.year,
                value: calendarData.calendar.text
            });
        } else {
            obj.value = calendarData.calendar.text;
            obj.addedAt = Date.now();
        }

        return data;
    });
    write();

    async function fetchJson() {
        let resp = await fetch(API_HOST + "/calendar", {
            method: "POST",
            headers: {
                Accept: "application/json",
            },
            body: JSON.stringify(state.jobData)
        });

        if (resp.status === 200) {
            const json = await resp.json();
            const blob = new Blob([(json as ApiCalendarCreatedResponseV1).text], {type: "application/json"});

            let anchor = document.createElement("a");
            anchor.href = window.URL.createObjectURL(blob);
            anchor.target = "_blank";

            anchor.click();
        }
    }

    return (
        <MaxWidthHelper size="md">
            <DownloadCard headingText="Success!"
                          bodyText="Calendar events have been generated and can be downloaded using the button below. The file can be imported by most (if not all) calendar applications."
                          fileName={fileName}
                          calendarData={calendarData.calendar.text}>
                <div class="space-y-2 mb-3">
                    <div>
                        <p>This calendar file can also be accessed from the following link:</p>
                        <code><a href={quickUrl}>{quickUrl}</a></code>
                    </div>

                    <p class="mb-3 font-bold text-xs lowercase cursor-pointer"
                       onclick={() => fetchJson()}>(Alternatively,
                        click here to open as JSON)</p>
                </div>
            </DownloadCard>
        </MaxWidthHelper>
    )
}

export default function GenerateComponent() {
    return (
        <LocalStorageProvider key={RECENT_SCANS_KEY} data={EMPTY_DATASTORE}>
            <GenerateComponentInternal/>
        </LocalStorageProvider>
    );
};