import {ApiJobMetadataV1} from "../../rest/generated/ApiJobMetadataV1";
import {useParams} from "solid-app-router";
import ResultOcrComponent from "./ResultOcrComponent";
import {ApiJobFinishedResponseV1} from "../../rest/generated/ApiJobFinishedResponseV1";
import {Dynamic} from "solid-js/web";
import {JobStatusProvider, useJobStatus} from "../../util/JobStatusProvider";
import ProcessingComponent from "./JobBackendComponent";
import GenerateComponent from "./GenerateComponent";
import JobInvalidComponent from "./JobInvalidComponent";
import JobFailedComponent from "./JobFailedComponent";
import {ApiJobFailedResponseV1} from "../../rest/generated/ApiJobFailedResponseV1";
import {WS_HOST} from "../../static";
import MaxWidthHelper from "../../components/MaxWidthHelper";

export const JOB_INTERNAL_STATE_WAITING = 0;
export const JOB_INTERNAL_STATE_PROCESSING = 1;
export const JOB_INTERNAL_STATE_RESULT_OCR = 2;
export const JOB_INTERNAL_STATE_DOWNLOAD_CALENDAR = 3;
export const JOB_INTERNAL_STATE_INVALID = 4;
export const JOB_INTERNAL_STATE_FAILED = 5;

export default function Job() {
    return (
        <JobStatusProvider componentState={JOB_INTERNAL_STATE_WAITING}
                           backendState={"Queued"}
                           jobData={null}
                           calendarData={null}>
            <JobInternal/>
        </JobStatusProvider>
    );
}

function JobInternal() {
    const params = useParams();
    let jobId = params.id;

    const [state, {updateComponentState, updateBackendState, updateJobData}] = useJobStatus();

    let socket = new WebSocket(WS_HOST + "/job/" + jobId);
    socket.addEventListener("message", (event) => {
        let eventJson = JSON.parse(event.data);

        let metadata = eventJson["metadata"] as ApiJobMetadataV1;
        if (metadata.state !== "INVALID") {
            updateComponentState(JOB_INTERNAL_STATE_PROCESSING);
        }

        switch (metadata.state) {
            case "CREATED":

                break;
            case "FAILED":
                socket.close();

                let error = eventJson as ApiJobFailedResponseV1;
                updateJobData(error.data);
                updateComponentState(JOB_INTERNAL_STATE_FAILED);
                break;
            case "INVALID":
                socket.close();

                updateComponentState(JOB_INTERNAL_STATE_INVALID);
                break;
            case "RUNNING":
                updateBackendState("Analysing image");
                break;
            case "FINISHED":
                socket.close();

                let finished = eventJson as ApiJobFinishedResponseV1;
                updateJobData(finished.data);
                updateComponentState(JOB_INTERNAL_STATE_RESULT_OCR);

                break;
        }
    });

    const WaitingComponent = () => <MaxWidthHelper size="xs">
        <div class="flex justify-center">
            <div class="flex justify-center items-center rounded-xl bg-pink-200 text-pink-600 px-3 py-3">
                <svg class="w-5 h-5 mr-1 fill-current animate-spin" xmlns="http://www.w3.org/2000/svg" width="20px"
                     height="20px" viewBox="0 0 24 24">
                    <path d="M0 0h24v24H0V0z" fill="none"/>
                    <path
                        d="M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z"/>
                </svg>
                <span class="lowercase">Loading data</span>
            </div>
        </div>
    </MaxWidthHelper>;

    const COMPONENT_STATE = {
        [JOB_INTERNAL_STATE_WAITING]: WaitingComponent,
        [JOB_INTERNAL_STATE_PROCESSING]: ProcessingComponent,
        [JOB_INTERNAL_STATE_RESULT_OCR]: ResultOcrComponent,
        [JOB_INTERNAL_STATE_DOWNLOAD_CALENDAR]: GenerateComponent,
        [JOB_INTERNAL_STATE_INVALID]: JobInvalidComponent,
        [JOB_INTERNAL_STATE_FAILED]: JobFailedComponent,
    };

    return (
        <Dynamic component={COMPONENT_STATE[state.componentState]}/>
    );
}