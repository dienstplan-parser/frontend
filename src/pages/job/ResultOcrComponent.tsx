import LabelChip from "../../components/LabelChip";
import {Index, Show} from "solid-js";
import {getMonthList} from "../../util/date";
import {TextField} from "../../components/TextField";
import TinyRoundButton from "../../components/TinyRoundButton";
import BackArrowLeft from "../../components/icon/ArrowLeftIcon";
import CheckMarkIcon from "../../components/icon/CheckMarkIcon";
import GreenButton from "../../components/GreenButton";
import {ApiJobDataV1} from "../../rest/generated/ApiJobDataV1";
import {ApiCalendarCreatedResponseV1} from "../../rest/generated/ApiCalendarCreatedResponseV1";
import {Link} from "solid-app-router";
import {useJobStatus} from "../../util/JobStatusProvider";
import {ApiCalendarCreatedWrapped} from "../../rest/ApiCalendarCreatedWrapped";
import {JOB_INTERNAL_STATE_DOWNLOAD_CALENDAR} from "./[id]";
import {API_HOST} from "../../static";
import JobNoDataFoundComponent from "./JobNoDataFoundComponent";
import {FormControl, FormGroup, withControl} from "rx-controls-solid";
import {toSignal} from "../../utils";
import {ApiCalendarCreatedResponseV2} from "../../rest/generated/ApiCalendarCreatedResponseV2";

const MONTH_NAMES: string[] = getMonthList();
const DIENST_REGEXES = [
    new RegExp("^\\d{1,2}\\s{1}.*\\s{1}\\d{1}\\s{1}\\d{2}:\\d{2}\\s?-\\s?\\d{2}:\\d{2}\\s{1}\\d{1,2},\\d{2}$"),
    new RegExp("^\\d{1,2}\\s{1}.*\\s{1}frei\\s{1}\\d{1}$"),
    new RegExp("^\\d{1,2}\\s{1}.*\\s{1}Urlaub\\s{1}\\d{1}\\s{1}\\d{1,2},\\d{2}$")
];

const YEAR_REGEX = new RegExp("^\\d*$");

const MONTH_FIELD = "month";
const YEAR_FIELD = "year";
const SCHEDULE_FIELD = "schedule-";

const lineOptions = {
    validators: [(c) => c.value != null && typeof c.value === "string" && DIENST_REGEXES.find(re => re.test(c.value.toString())) !== undefined ? null : {
        message: "Line does not match any format!",
        default: true,
    }]
};

export default function ResultOcrComponent() {
    const [state, {updateComponentState, updateJobData, updateCalendar}] = useJobStatus();
    let jobData = state.jobData as ApiJobDataV1;

    const startSubmit = (event) => {
        //ignore result
        submit(event.target);
        event.preventDefault();
        return false;
    };

    const submit = async (form: HTMLFormElement) => {
        let formData = new FormData(form);
        let year = parseInt(formData.get(YEAR_FIELD).toString());
        let month = parseInt(formData.get(MONTH_FIELD).toString());

        let schedule = Array.from(formData.keys()).filter(value => value.startsWith(SCHEDULE_FIELD)).map(key => formData.get(key).toString());

        let data = new ApiJobDataV1(
            year, month, schedule
        )

        let response = await fetch(API_HOST + "/calendar", {
            method: "POST",
            headers: {
                Accept: "text/calendar",
            },
            body: JSON.stringify(data)
        });

        let json = await response.json();
        if (response.status === 200) {
            updateCalendar(new ApiCalendarCreatedWrapped(
                json as ApiCalendarCreatedResponseV2,
                month, year
            ));

            updateJobData(data);
            updateComponentState(JOB_INTERNAL_STATE_DOWNLOAD_CALENDAR);
        }
    };

    const controls = {
        "year": new FormControl<number | null>(null, {
            validators: [(c) => c.value !== null && YEAR_REGEX.test(c.value.toString()) ? null : {
                message: "Please enter a year!",
                default: true,
            }]
        })
    };

    for (let i = 0; i < jobData.schedule.length; i++) {
        const control = new FormControl<string | null>(null, lineOptions);
        control.setValue(jobData.schedule[i]);

        controls[`line-${i}`] = control;
    }

    const controlFactoryFunction = (props) => new FormGroup(controls);

    const OcrForm = withControl<{
        control: ReturnType<typeof controlFactoryFunction>,
        label?: string,
    }>({
        controlFactory: controlFactoryFunction,
        component: (props) => {
            const control = props.control;
            const yearControl = control.get("year");

            const controlInvalid = toSignal(control.observe("invalid"));
            const controlPending = toSignal(control.observe("pending"));

            yearControl.setValue(jobData.year);

            return (
                <Show when={jobData.schedule.length > 0} fallback={<JobNoDataFoundComponent/>}>
                    <form method="post" onsubmit={startSubmit} action="#" class="max-w-6xl mx-auto">
                        <LabelChip text="Period" textRight="Is this correct?"/>

                        <select name={MONTH_FIELD}
                                class="px-3 py-3 mb-3 placeholder-blueGray-300 text-black relative bg-white rounded-lg text-sm focus:outline-none w-full border-2 focus:border-pink-500">
                            <Index each={MONTH_NAMES}>{(monthName, i) =>
                                <option selected={i + 1 === jobData.month}
                                        value={(i + 1).toString()}>{monthName()}</option>
                            }</Index>
                        </select>

                        <TextField controlName="year" type="number" name={YEAR_FIELD}
                                   required={true}/>

                        <LabelChip text="Plan"/>

                        <div class="text-white px-6 pt-5 border-0 rounded-lg relative mb-4 bg-orange-500"
                             style="background-color: #ed8936;">
                        <span class="mr-8">
                            Please make sure each line matches one of the following formats <b>EXACTLY</b>:
                            <ul>
                                <li>
                                    <code>%DAY_OF_MONTH% %DAY_NAME% frei %FOLGE%</code>
                                </li>
                                <li>
                                    <code>%DAY_OF_MONTH% %DAY_NAME% %DIENST_NAME% %FOLGE% %FROM% - %TILL% %HOURS%</code>
                                </li>
                            </ul>

                            Examples:

                            <ul>
                                <li>
                                    <code>1 Mittwoch frei 1</code>
                                </li>
                                <li>
                                    <code>2 Donnerstag Zivildiender Früh 1 07:00 - 15:30 8,00</code>
                                </li>
                            </ul>
                        </span>
                        </div>

                        <ul class="mb-3">
                            <Index each={jobData.schedule}>{(line, i) =>
                                <li>
                                    <TextField controlName={`line-${i}`} type="text" name={SCHEDULE_FIELD + i}
                                               required={true}/>
                                </li>
                            }</Index>
                        </ul>

                        <div class="flex">
                            <Link href="/" replace={true}>
                                <TinyRoundButton text="Back to start" icon={<BackArrowLeft/>}/>
                            </Link>

                            <GreenButton type="submit" text="Looks good" icon={CheckMarkIcon}
                                         disabled={controlInvalid() || controlPending()}/>
                        </div>
                    </form>
                </Show>
            );
        }
    });

    return (<OcrForm/>)
};