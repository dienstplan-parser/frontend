import RedErrorCard from "../../components/RedErrorCard";
import MaxWidthHelper from "../../components/MaxWidthHelper";

export default function JobInvalidComponent() {
    return (
        <MaxWidthHelper size="md">
            <RedErrorCard>
                <h1 class="text-lg font-semibold">Job could not be found :/</h1>
                <p>The job you are trying to view either never existed or has already finished, maybe
                    try again?</p>
            </RedErrorCard>
        </MaxWidthHelper>
    );
};