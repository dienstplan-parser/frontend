import {useJobStatus} from "../../util/JobStatusProvider";
import {ApiJobFailureDataV1} from "../../rest/generated/ApiJobFailureDataV1";
import {Index} from "solid-js";
import RedErrorCard from "../../components/RedErrorCard";

export default function JobFailedComponent() {
    const [state, {}] = useJobStatus();
    let data = state.jobData as ApiJobFailureDataV1;

    return (
        <RedErrorCard>
            <h1 class="text-lg font-semibold">Job execution failed :(</h1>
            <p class="">Error saved to <code>{data.errorFile}</code></p>
            <div class="mt-5">
                <Index each={data.error}>
                    {(errorLine, i) =>
                        <div><code>{errorLine}</code></div>
                    }
                </Index>
            </div>
        </RedErrorCard>
    );
};