import {defineConfig} from "vite";
import solidPlugin from "vite-plugin-solid";
import GitRevision from "vite-plugin-git-revision";

export default defineConfig({
    plugins: [
        solidPlugin(),
    ],
    build: {
        target: "esnext",
        polyfillDynamicImport: false,
    },
});